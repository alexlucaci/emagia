import heroes
import mobs
import battle

if __name__ == "__main__":
    hero_1 = heroes.Orderus()
    mob_1 = mobs.WildBeast()
    emagia = battle.Battle(hero_1, mob_1)
    emagia.start_battle()
