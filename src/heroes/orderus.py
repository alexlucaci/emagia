import random

import players
import skills


class Orderus(players.BasePlayer):
    health_interval = (70, 100)
    strength_interval = (70, 80)
    defence_interval = (45, 55)
    speed_interval = (40, 50)
    luck_interval = (10, 30)

    attack_skills = [skills.RapidStrike]
    defend_skills = [skills.MagicShield]
