import players


class WildBeast(players.BasePlayer):
    health_interval = (60, 90)
    strength_interval = (60, 90)
    defence_interval = (40, 60)
    speed_interval = (40, 60)
    luck_interval = (25, 40)
