class Battle:
    max_rounds = 20

    def __init__(self, player_1, player_2):
        self.player_1 = player_1
        self.player_2 = player_2

    def start_battle(self):
        print("Player stats: ")
        print(self.player_1)
        print(self.player_2)
        positions = self.compute_positions(self.player_1, self.player_2)
        print(f"\nBattle starts: {positions[0].name} is the first to attack")
        battle_is_over = False
        for round_number in range(1, 21):
            print(f"\nRound {round_number}")
            print("----------")
            positions[0].attack(positions[1])

            if positions[1].health == 0:
                battle_is_over = True
                print(f"{positions[1].name} is dead")
                print("\nBattle is over")
                print("----------")
                print(f"Winner is {positions[0].name}")
                break

            positions[0], positions[1] = positions[1], positions[0]

        if not battle_is_over:
            print("Battle is over")
            print("----------")
            if positions[0].health > positions[1].health:
                print(f"Winner is {positions[0].name}")
            elif positions[1].health > positions[0].health:
                print(f"Winner is {positions[1].name}")
            else:
                print("It's a tie")

    @staticmethod
    def compute_positions(player_1, player_2):
        positions = [player_1, player_2]
        positions = sorted(positions, key=lambda player: (player.speed, player.luck), reverse=True)
        return positions
