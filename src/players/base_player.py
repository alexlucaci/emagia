import random
import typing

from abc import ABC, abstractmethod


class BasePlayer(ABC):

    @property
    @abstractmethod
    def health_interval(self) -> typing.Tuple[int, int]:
        raise NotImplementedError

    @property
    @abstractmethod
    def strength_interval(self) -> typing.Tuple[int, int]:
        raise NotImplementedError

    @property
    @abstractmethod
    def defence_interval(self) -> typing.Tuple[int, int]:
        raise NotImplementedError

    @property
    @abstractmethod
    def speed_interval(self) -> typing.Tuple[int, int]:
        raise NotImplementedError

    @property
    @abstractmethod
    def luck_interval(self) -> typing.Tuple[int, int]:
        raise NotImplementedError

    @property
    def attack_skills(self) -> typing.List:
        return []

    @property
    def defend_skills(self) -> typing.List:
        return []

    @property
    def name(self):
        return self.__class__.__name__

    def __init__(self):
        self.health = self.init_health()
        self.strength = self.init_strength()
        self.defence = self.init_defence()
        self.speed = self.init_speed()
        self.luck = self.init_luck()
        self.is_dead = False

    def init_health(self):
        return random.randint(*self.health_interval)

    def init_strength(self):
        return random.randint(*self.strength_interval)

    def init_defence(self):
        return random.randint(*self.defence_interval)

    def init_speed(self):
        return random.randint(*self.speed_interval)

    def init_luck(self):
        return random.randint(*self.luck_interval)

    def __str__(self):
        return f"{self.name} has the following stats " \
               f"\n health: {self.health} " \
               f"\n strength: {self.strength} " \
               f"\n defence: {self.defence} " \
               f"\n speed: {self.speed} " \
               f"\n luck: {self.luck}"

    def add_attack_skills(function):
        # will use a random skill (if exists) from self.attack_skills to decorate the attack method in order to apply
        # the special skill
        def wrapped(self, opponent):
            decorated_function = function
            if len(self.attack_skills) > 0:
                position_of_skill_used = random.randint(0, len(self.attack_skills) - 1)
                decorated_function = self.attack_skills[position_of_skill_used].use_skill(function)

            return decorated_function(self, opponent)

        return wrapped

    def add_defend_skills(function):
        # will use a random skill (if exists) from self.defend_skills to decorate the attack method in order to apply
        # the special skill
        def wrapped(self, strength):
            decorated_function = function
            if len(self.defend_skills) > 0:
                position_of_skill_used = random.randint(0, len(self.defend_skills) - 1)
                decorated_function = self.defend_skills[position_of_skill_used].use_skill(function)

            return decorated_function(self, strength)

        return wrapped

    @add_attack_skills
    def attack(self, oponent: 'BasePlayer'):
        print(f"{self.name} attacks {oponent.name}")
        oponent.defend(self.strength)

    def defend(self, enemy_strength):
        print(f"{self.name} with {self.health} health and {self.defence} defence defends from enemy_strength: {enemy_strength}")
        damage = 0

        if not self.is_lucky():
            damage = self.compute_damage(enemy_strength)
        else:
            print(f"{self.name} is lucky and dodges the attack")

        if damage >= self.health:
            self.health = 0
        else:
            self.health -= damage

        if self.health == 0:
            self.is_dead = True

        print(f"{self.name} receives {damage} damage and now has {self.health} health ")

    @add_defend_skills
    def compute_damage(self, strength):
        if strength <= self.defence:
            return 0
        return strength - self.defence

    def is_lucky(self):
        return random.randint(1, 100) <= self.luck
