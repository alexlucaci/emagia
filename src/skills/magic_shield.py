from .base_skill import BaseSkill


class MagicShield(BaseSkill):
    chance_to_hit = 20

    @classmethod
    def use_skill(cls, func):
        # will decorate the defend function and return half of the damage that was supposed to return if
        # the skill is lucky
        def wrapped(self, strength):
            damage = func(self, strength)
            if cls.is_lucky():
                damage /= 2
            return damage

        return wrapped
