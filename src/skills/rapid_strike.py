from .base_skill import BaseSkill


class RapidStrike(BaseSkill):
    chance_to_hit = 10

    @classmethod
    def use_skill(cls, func):
        # will call the attack skill one more if the skill is lucky and after that it resumes to normal flow
        def wrapped(self, opponent):
            if cls.is_lucky():
                func(self, opponent)
            return func(self, opponent)

        return wrapped
