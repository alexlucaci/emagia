import random

from abc import ABC, abstractmethod


class BaseSkill(ABC):

    @property
    @abstractmethod
    def chance_to_hit(self) -> int:
        raise NotImplementedError

    @classmethod
    def is_lucky(cls):
        has_luck = random.randint(0, 100) <= cls.chance_to_hit
        print(f"Skill {cls.__name__} is casting ... casted {has_luck}")
        return has_luck

    @classmethod
    @abstractmethod
    def use_skill(cls, func):
        raise NotImplementedError
