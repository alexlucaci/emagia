import random

from unittest import mock

import heroes
import mobs

from .test_mob import TestMob


class TestOrderus(TestMob):
    def setup_method(self):
        self.player = heroes.Orderus()

        # make sure that no special skills will be used
        self.player.attack_skills[0].is_lucky = mock.MagicMock(return_value=False)
        self.player.defend_skills[0].is_lucky = mock.MagicMock(return_value=False)

        # make sure that the player has 0 luck and cannot dodge attacks
        self.player.luck = 0

    def test_orderus_with_rapid_strike(self):
        self.player.attack_skills[0].is_lucky = mock.MagicMock(return_value=True)

        wild_beast = mobs.WildBeast()
        wild_beast.luck = 0

        damage_per_hit = self.player.strength - wild_beast.defence
        wild_beast_initial_health = wild_beast.health

        self.player.attack(wild_beast)
        wild_beast_health_after_attack = wild_beast_initial_health - damage_per_hit * 2
        wild_beast_health_after_attack = 0 if wild_beast_health_after_attack < 0 else wild_beast_health_after_attack
        assert wild_beast.health == wild_beast_health_after_attack

    def test_mob_is_dead_damage_is_greater_than_health_with_rapid_strike(self):
        self.player.attack_skills[0].is_lucky = mock.MagicMock(return_value=True)

        wild_beast = mobs.WildBeast()
        wild_beast.luck = 0

        self.player.strength = random.randint(wild_beast.health + wild_beast.defence, 400)
        self.player.attack(wild_beast)
        assert wild_beast.is_dead is True

    def test_mob_is_dead_damage_is_equal_with_health_with_rapid_strike(self):
        self.player.attack_skills[0].is_lucky = mock.MagicMock(return_value=True)

        wild_beast = mobs.WildBeast()
        wild_beast.luck = 0

        self.player.strength = (wild_beast.health + wild_beast.defence) * 2
        self.player.attack(wild_beast)
        assert wild_beast.is_dead is True

    def test_player_defends_with_magic_shield(self):
        self.player.defend_skills[0].is_lucky = mock.MagicMock(return_value=True)
        self.player.luck = 0

        wild_beast = mobs.WildBeast()

        base_damage = wild_beast.strength - self.player.defence
        initial_health = self.player.health

        wild_beast.attack(self.player)

        assert self.player.health == (initial_health - base_damage / 2)


