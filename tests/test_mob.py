import random

import pytest

import mobs


class TestMob:

    def setup_method(self):
        self.player = mobs.WildBeast()

        # make sure that the beast has 0 luck and cannot dodge attacks
        self.player.luck = 0

    def test_compute_damage_strength_greater_than_defence(self):
        enemy_strength = random.randint(self.player.defence, 150)
        assert self.player.compute_damage(enemy_strength) == enemy_strength - self.player.defence

    def test_compute_damage_strength_less_than_defence(self):
        enemy_strength = random.randint(0, self.player.defence)
        assert self.player.compute_damage(enemy_strength) == 0

    def test_attack_fails_when_defender_is_lucky(self):
        another_wild_beast = mobs.WildBeast()

        # make sure that the beast is always lucky
        self.player.luck = 100
        old_health = self.player.health
        another_wild_beast.attack(self.player)
        assert self.player.health == old_health

    def test_mob_is_dead_damage_is_greater_than_health(self):
        another_wild_beast = mobs.WildBeast()
        another_wild_beast.strength = random.randint(self.player.health + self.player.defence, 400)
        another_wild_beast.attack(self.player)
        assert self.player.is_dead is True

    def test_mob_is_dead_damage_is_equal_with_health(self):
        another_wild_beast = mobs.WildBeast()
        another_wild_beast.strength = self.player.health + self.player.defence
        another_wild_beast.attack(self.player)
        assert self.player.is_dead is True

    def test_mob_is_not_dead_damage_is_less_than_health(self):
        another_wild_beast = mobs.WildBeast()
        another_wild_beast.strength = random.randint(0, self.player.health + self.player.defence - 1)
        another_wild_beast.attack(self.player)
        assert self.player.is_dead is False

    def test_health_init(self):
        assert self.player.health_interval[0] <= self.player.health <= self.player.health_interval[1]

    def test_strength_init(self):
        assert self.player.strength_interval[0] <= self.player.strength <= self.player.strength_interval[1]

    def test_defence_init(self):
        assert self.player.defence_interval[0] <= self.player.defence <= self.player.defence_interval[1]

    def test_speed_init(self):
        assert self.player.speed_interval[0] <= self.player.speed <= self.player.speed_interval[1]

    def test_luck_init(self):
        self.player.luck = self.player.init_luck()
        assert self.player.luck_interval[0] <= self.player.luck <= self.player.luck_interval[1]
