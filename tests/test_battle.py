import heroes
import mobs

import battle


class TestBattle:
    def test_player_1_starts_greater_speed(self):
        player_1 = heroes.Orderus()
        player_2 = mobs.WildBeast()

        player_1.speed = 15
        player_2.speed = 10

        assert battle.Battle.compute_positions(player_1, player_2)[0] == player_1

    def test_player_2_starts_greater_speed(self):
        player_1 = heroes.Orderus()
        player_2 = mobs.WildBeast()

        player_1.speed = 20
        player_2.speed = 25

        assert battle.Battle.compute_positions(player_1, player_2)[0] == player_2

    def test_player_1_starts_equal_speed_greater_luck(self):
        player_1 = heroes.Orderus()
        player_2 = mobs.WildBeast()

        player_1.speed = 20
        player_2.speed = 20

        player_1.luck = 15
        player_2.luck = 10

        assert battle.Battle.compute_positions(player_1, player_2)[0] == player_1

    def test_player_2_starts_equal_speed_greater_luck(self):
        player_1 = heroes.Orderus()
        player_2 = mobs.WildBeast()

        player_1.speed = 20
        player_2.speed = 20

        player_1.speed = 20
        player_2.speed = 25

        assert battle.Battle.compute_positions(player_1, player_2)[0] == player_2
